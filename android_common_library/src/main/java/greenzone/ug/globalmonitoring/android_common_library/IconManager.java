package greenzone.ug.globalmonitoring.android_common_library;

/**
 * Created by Markus on 06.10.2017.
 */

public class IconManager {
    public static int findeIcon(String category, String risk){
        int LocationDrawable = R.drawable.icon_rot;
        if(category.equals("travelwarning") && risk.equals("1")){
            LocationDrawable = R.drawable.aa_r1_map;
        }
        if(category.equals("travelwarning") && risk.equals("2")){
            LocationDrawable = R.drawable.aa_r2_map;
        }
        if(category.equals("travelwarning") && risk.equals("3")){
            LocationDrawable = R.drawable.aa_r3_map;
        }
        if(category.equals("travelwarning") && risk.equals("4")){
            LocationDrawable = R.drawable.aa_r4_map;
        }
        if(category.equals("travelwarning") && risk.equals("5")){
            LocationDrawable = R.drawable.aa_r5_map;
        }
        if(category.equals("animalattack") && risk.equals("1")){
            LocationDrawable = R.drawable.animal_attack_r1_map;
        }
        if(category.equals("animalattack") && risk.equals("2")){
            LocationDrawable = R.drawable.animal_attack_r2_map;
        }
        if(category.equals("animalattack") && risk.equals("3")){
            LocationDrawable = R.drawable.animal_attack_r3_map;
        }
        if(category.equals("animalattack") && risk.equals("4")){
            LocationDrawable = R.drawable.animal_attack_r4_map;
        }
        if(category.equals("animalattack") && risk.equals("5")){
            LocationDrawable = R.drawable.animal_attack_r5_map;
        }
        if(category.equals("armedconflict") && risk.equals("1")){
            LocationDrawable = R.drawable.armed_conflict_r1_map;
        }
        if(category.equals("armedconflict") && risk.equals("2")){
            LocationDrawable = R.drawable.armed_conflict_r2_map;
        }
        if(category.equals("armedconflict") && risk.equals("3")){
            LocationDrawable = R.drawable.armed_conflict_r3_map;
        }
        if(category.equals("armedconflict") && risk.equals("4")){
            LocationDrawable = R.drawable.armed_conflict_r4_map;
        }
        if(category.equals("armedconflict") && risk.equals("5")){
            LocationDrawable = R.drawable.armed_conflict_r5_map;
        }
        if(category.equals("coldwave") && risk.equals("1")){
            LocationDrawable = R.drawable.coldwave_r1_map;
        }
        if(category.equals("coldwave") && risk.equals("2")){
            LocationDrawable = R.drawable.coldwave_r2_map;
        }
        if(category.equals("coldwave") && risk.equals("3")){
            LocationDrawable = R.drawable.coldwave_r3_map;
        }
        if(category.equals("coldwave") && risk.equals("4")){
            LocationDrawable = R.drawable.coldwave_r4_map;
        }
        if(category.equals("coldwave") && risk.equals("5")){
            LocationDrawable = R.drawable.coldwave_r5_map;
        }
        if(category.equals("complexemergency") && risk.equals("1")){
            LocationDrawable = R.drawable.complexemergency_r1_map;
        }
        if(category.equals("complexemergency") && risk.equals("2")){
            LocationDrawable = R.drawable.complexemergency_r2_map;
        }
        if(category.equals("complexemergency") && risk.equals("3")){
            LocationDrawable = R.drawable.complexemergency_r3_map;
        }
        if(category.equals("complexemergency") && risk.equals("4")){
            LocationDrawable = R.drawable.complexemergency_r4_map;
        }
        if(category.equals("complexemergency") && risk.equals("5")){
            LocationDrawable = R.drawable.complexemergency_r5_map;
        }
        if(category.equals("crime") && risk.equals("1")){
            LocationDrawable = R.drawable.crime_r1_map;
        }
        if(category.equals("crime") && risk.equals("2")){
            LocationDrawable = R.drawable.crime_r2_map;
        }
        if(category.equals("crime") && risk.equals("3")){
            LocationDrawable = R.drawable.crime_r3_map;
        }
        if(category.equals("crime") && risk.equals("4")){
            LocationDrawable = R.drawable.crime_r4_map;
        }
        if(category.equals("crime") && risk.equals("5")){
            LocationDrawable = R.drawable.crime_r5_map;
        }
        if(category.equals("demonstration") && risk.equals("1")){
            LocationDrawable = R.drawable.demonstration_r1_map;
        }
        if(category.equals("demonstration") && risk.equals("2")){
            LocationDrawable = R.drawable.demonstration_r2_map;
        }
        if(category.equals("demonstration") && risk.equals("3")){
            LocationDrawable = R.drawable.demonstration_r3_map;
        }
        if(category.equals("demonstration") && risk.equals("4")){
            LocationDrawable = R.drawable.demonstration_r4_map;
        }
        if(category.equals("demonstration") && risk.equals("5")){
            LocationDrawable = R.drawable.demonstration_r5_map;
        }
        if(category.equals("drought") && risk.equals("1")){
            LocationDrawable = R.drawable.drought_r1_map;
        }
        if(category.equals("drought") && risk.equals("2")){
            LocationDrawable = R.drawable.drought_r2_map;
        }
        if(category.equals("drought") && risk.equals("3")){
            LocationDrawable = R.drawable.drought_r3_map;
        }
        if(category.equals("drought") && risk.equals("4")){
            LocationDrawable = R.drawable.drought_r4_map;
        }
        if(category.equals("drought") && risk.equals("5")){
            LocationDrawable = R.drawable.drought_r5_map;
        }
        if(category.equals("earthquake") && risk.equals("1")){
            LocationDrawable = R.drawable.earthquake_r1_map;
        }
        if(category.equals("earthquake") && risk.equals("2")){
            LocationDrawable = R.drawable.earthquake_r2_map;
        }
        if(category.equals("earthquake") && risk.equals("3")){
            LocationDrawable = R.drawable.earthquake_r3_map;
        }
        if(category.equals("earthquake") && risk.equals("4")){
            LocationDrawable = R.drawable.earthquake_r4_map;
        }
        if(category.equals("earthquake") && risk.equals("5")){
            LocationDrawable = R.drawable.earthquake_r5_map;
        }
        if(category.equals("envpollution") && risk.equals("1")){
            LocationDrawable = R.drawable.envpollution_r1_map;
        }
        if(category.equals("envpollution") && risk.equals("2")){
            LocationDrawable = R.drawable.envpollution_r2_map;
        }
        if(category.equals("envpollution") && risk.equals("3")){
            LocationDrawable = R.drawable.envpollution_r3_map;
        }
        if(category.equals("envpollution") && risk.equals("4")){
            LocationDrawable = R.drawable.envpollution_r4_map;
        }
        if(category.equals("envpollution") && risk.equals("5")){
            LocationDrawable = R.drawable.envpollution_r5_map;
        }
        if(category.equals("epidemic") && risk.equals("1")){
            LocationDrawable = R.drawable.epidemic_r1_map;
        }
        if(category.equals("epidemic") && risk.equals("2")){
            LocationDrawable = R.drawable.epidemic_r2_map;
        }
        if(category.equals("epidemic") && risk.equals("3")){
            LocationDrawable = R.drawable.epidemic_r3_map;
        }
        if(category.equals("epidemic") && risk.equals("4")){
            LocationDrawable = R.drawable.epidemic_r4_map;
        }
        if(category.equals("epidemic") && risk.equals("5")){
            LocationDrawable = R.drawable.epidemic_r5_map;
        }
        if(category.equals("explosion") && risk.equals("1")){
            LocationDrawable = R.drawable.explosion_r1_map;
        }
        if(category.equals("explosion") && risk.equals("2")){
            LocationDrawable = R.drawable.explosion_r2_map;
        }
        if(category.equals("explosion") && risk.equals("3")){
            LocationDrawable = R.drawable.explosion_r3_map;
        }
        if(category.equals("explosion") && risk.equals("4")){
            LocationDrawable = R.drawable.explosion_r4_map;
        }
        if(category.equals("explosion") && risk.equals("5")){
            LocationDrawable = R.drawable.explosion_r5_map;
        }
        if(category.equals("flashflood") && risk.equals("1")){
            LocationDrawable = R.drawable.flashflood_r1_map;
        }
        if(category.equals("flashflood") && risk.equals("2")){
            LocationDrawable = R.drawable.flashflood_r2_map;
        }
        if(category.equals("flashflood") && risk.equals("3")){
            LocationDrawable = R.drawable.flashflood_r3_map;
        }
        if(category.equals("flashflood") && risk.equals("4")){
            LocationDrawable = R.drawable.flashflood_r4_map;
        }
        if(category.equals("flashflood") && risk.equals("5")){
            LocationDrawable = R.drawable.flashflood_r5_map;
        }
        if(category.equals("flood") && risk.equals("1")){
            LocationDrawable = R.drawable.flood_r1_map;
        }
        if(category.equals("flood") && risk.equals("2")){
            LocationDrawable = R.drawable.flood_r2_map;
        }
        if(category.equals("flood") && risk.equals("3")){
            LocationDrawable = R.drawable.flood_r3_map;
        }
        if(category.equals("flood") && risk.equals("4")){
            LocationDrawable = R.drawable.flood_r4_map;
        }
        if(category.equals("flood") && risk.equals("5")){
            LocationDrawable = R.drawable.flood_r5_map;
        }
        if(category.equals("hazmataccident") && risk.equals("1")){
            LocationDrawable = R.drawable.hazmataccident_r1_map;
        }
        if(category.equals("hazmataccident") && risk.equals("2")){
            LocationDrawable = R.drawable.hazmataccident_r2_map;
        }
        if(category.equals("hazmataccident") && risk.equals("3")){
            LocationDrawable = R.drawable.hazmataccident_r3_map;
        }
        if(category.equals("hazmataccident") && risk.equals("4")){
            LocationDrawable = R.drawable.hazmataccident_r4_map;
        }
        if(category.equals("hazmataccident") && risk.equals("5")){
            LocationDrawable = R.drawable.hazmataccident_r5_map;
        }
        if(category.equals("heatwave") && risk.equals("1")){
            LocationDrawable = R.drawable.heatwave_r1_map;
        }
        if(category.equals("heatwave") && risk.equals("2")){
            LocationDrawable = R.drawable.heatwave_r2_map;
        }
        if(category.equals("heatwave") && risk.equals("3")){
            LocationDrawable = R.drawable.heatwave_r3_map;
        }
        if(category.equals("heatwave") && risk.equals("4")){
            LocationDrawable = R.drawable.heatwave_r4_map;
        }
        if(category.equals("heatwave") && risk.equals("5")){
            LocationDrawable = R.drawable.heatwave_r5_map;
        }
        if(category.equals("importantdate") && risk.equals("1")){
            LocationDrawable = R.drawable.importantdate_r1_map;
        }
        if(category.equals("importantdate") && risk.equals("2")){
            LocationDrawable = R.drawable.importantdate_r2_map;
        }
        if(category.equals("importantdate") && risk.equals("3")){
            LocationDrawable = R.drawable.importantdate_r3_map;
        }
        if(category.equals("importantdate") && risk.equals("4")){
            LocationDrawable = R.drawable.importantdate_r4_map;
        }
        if(category.equals("importantdate") && risk.equals("5")){
            LocationDrawable = R.drawable.importantdate_r5_map;
        }
        if(category.equals("kidnapping") && risk.equals("1")){
            LocationDrawable = R.drawable.kidnapping_r1_map;
        }
        if(category.equals("kidnapping") && risk.equals("2")){
            LocationDrawable = R.drawable.kidnapping_r2_map;
        }
        if(category.equals("kidnapping") && risk.equals("3")){
            LocationDrawable = R.drawable.kidnapping_r3_map;
        }
        if(category.equals("kidnapping") && risk.equals("4")){
            LocationDrawable = R.drawable.kidnapping_r4_map;
        }
        if(category.equals("kidnapping") && risk.equals("5")){
            LocationDrawable = R.drawable.kidnapping_r5_map;
        }
        if(category.equals("landslide") && risk.equals("1")){
            LocationDrawable = R.drawable.landslide_r1_map;
        }
        if(category.equals("landslide") && risk.equals("2")){
            LocationDrawable = R.drawable.landslide_r2_map;
        }
        if(category.equals("landslide") && risk.equals("3")){
            LocationDrawable = R.drawable.landslide_r3_map;
        }
        if(category.equals("landslide") && risk.equals("4")){
            LocationDrawable = R.drawable.landslide_r4_map;
        }
        if(category.equals("landslide") && risk.equals("5")){
            LocationDrawable = R.drawable.landslide_r5_map;
        }
        if(category.equals("nocategory") && risk.equals("1")){
            LocationDrawable = R.drawable.nocategory_r1_map;
        }
        if(category.equals("nocategory") && risk.equals("2")){
            LocationDrawable = R.drawable.nocategory_r2_map;
        }
        if(category.equals("nocategory") && risk.equals("3")){
            LocationDrawable = R.drawable.nocategory_r3_map;
        }
        if(category.equals("nocategory") && risk.equals("4")){
            LocationDrawable = R.drawable.nocategory_r4_map;
        }
        if(category.equals("nocategory") && risk.equals("5")){
            LocationDrawable = R.drawable.nocategory_r5_map;
        }
        if(category.equals("nuclearevent") && risk.equals("1")){
            LocationDrawable = R.drawable.nuclearevent_r1_map;
        }
        if(category.equals("nuclearevent") && risk.equals("2")){
            LocationDrawable = R.drawable.nuclearevent_r2_map;
        }
        if(category.equals("nuclearevent") && risk.equals("3")){
            LocationDrawable = R.drawable.nuclearevent_r3_map;
        }
        if(category.equals("nuclearevent") && risk.equals("4")){
            LocationDrawable = R.drawable.nuclearevent_r4_map;
        }
        if(category.equals("nuclearevent") && risk.equals("5")){
            LocationDrawable = R.drawable.nuclearevent_r5_map;
        }
        if(category.equals("personalinjury") && risk.equals("1")){
            LocationDrawable = R.drawable.personalinjury_r1_map;
        }
        if(category.equals("personalinjury") && risk.equals("2")){
            LocationDrawable = R.drawable.personalinjury_r2_map;
        }
        if(category.equals("personalinjury") && risk.equals("3")){
            LocationDrawable = R.drawable.personalinjury_r3_map;
        }
        if(category.equals("personalinjury") && risk.equals("4")){
            LocationDrawable = R.drawable.personalinjury_r4_map;
        }
        if(category.equals("personalinjury") && risk.equals("5")){
            LocationDrawable = R.drawable.personalinjury_r5_map;
        }
        if(category.equals("pestoutbreak") && risk.equals("1")){
            LocationDrawable = R.drawable.pestoutbreak_r1_map;
        }
        if(category.equals("pestoutbreak") && risk.equals("2")){
            LocationDrawable = R.drawable.pestoutbreak_r2_map;
        }
        if(category.equals("pestoutbreak") && risk.equals("3")){
            LocationDrawable = R.drawable.pestoutbreak_r3_map;
        }
        if(category.equals("pestoutbreak") && risk.equals("4")){
            LocationDrawable = R.drawable.pestoutbreak_r4_map;
        }
        if(category.equals("pestoutbreak") && risk.equals("5")){
            LocationDrawable = R.drawable.pestoutbreak_r5_map;
        }
        if(category.equals("piracy") && risk.equals("1")){
            LocationDrawable = R.drawable.piracy_r1_map;
        }
        if(category.equals("piracy") && risk.equals("2")){
            LocationDrawable = R.drawable.piracy_r2_map;
        }
        if(category.equals("piracy") && risk.equals("3")){
            LocationDrawable = R.drawable.piracy_r3_map;
        }
        if(category.equals("piracy") && risk.equals("4")){
            LocationDrawable = R.drawable.piracy_r4_map;
        }
        if(category.equals("piracy") && risk.equals("5")){
            LocationDrawable = R.drawable.piracy_r5_map;
        }
        if(category.equals("poisoning") && risk.equals("1")){
            LocationDrawable = R.drawable.poisoning_r1_map;
        }
        if(category.equals("poisoning") && risk.equals("2")){
            LocationDrawable = R.drawable.poisoning_r2_map;
        }
        if(category.equals("poisoning") && risk.equals("3")){
            LocationDrawable = R.drawable.poisoning_r3_map;
        }
        if(category.equals("poisoning") && risk.equals("4")){
            LocationDrawable = R.drawable.poisoning_r4_map;
        }
        if(category.equals("poisoning") && risk.equals("5")){
            LocationDrawable = R.drawable.poisoning_r5_map;
        }
        if(category.equals("politicalcrisis") && risk.equals("1")){
            LocationDrawable = R.drawable.politicalcrisis_r1_map;
        }
        if(category.equals("politicalcrisis") && risk.equals("2")){
            LocationDrawable = R.drawable.politicalcrisis_r2_map;
        }
        if(category.equals("politicalcrisis") && risk.equals("3")){
            LocationDrawable = R.drawable.politicalcrisis_r3_map;
        }
        if(category.equals("politicalcrisis") && risk.equals("4")){
            LocationDrawable = R.drawable.politicalcrisis_r4_map;
        }
        if(category.equals("politicalcrisis") && risk.equals("5")){
            LocationDrawable = R.drawable.politicalcrisis_r5_map;
        }
        if(category.equals("poweroutage") && risk.equals("1")){
            LocationDrawable = R.drawable.poweroutage_r1_map;
        }
        if(category.equals("poweroutage") && risk.equals("2")){
            LocationDrawable = R.drawable.poweroutage_r2_map;
        }
        if(category.equals("poweroutage") && risk.equals("3")){
            LocationDrawable = R.drawable.poweroutage_r3_map;
        }
        if(category.equals("poweroutage") && risk.equals("4")){
            LocationDrawable = R.drawable.poweroutage_r4_map;
        }
        if(category.equals("poweroutage") && risk.equals("5")){
            LocationDrawable = R.drawable.poweroutage_r5_map;
        }
        if(category.equals("severeweather") && risk.equals("1")){
            LocationDrawable = R.drawable.severeweather_r1_map;
        }
        if(category.equals("severeweather") && risk.equals("2")){
            LocationDrawable = R.drawable.severeweather_r2_map;
        }
        if(category.equals("severeweather") && risk.equals("3")){
            LocationDrawable = R.drawable.severeweather_r3_map;
        }
        if(category.equals("severeweather") && risk.equals("4")){
            LocationDrawable = R.drawable.severeweather_r4_map;
        }
        if(category.equals("severeweather") && risk.equals("5")){
            LocationDrawable = R.drawable.severeweather_r5_map;
        }
        if(category.equals("snowavalanche") && risk.equals("1")){
            LocationDrawable = R.drawable.snowavalanche_r1_map;
        }
        if(category.equals("snowavalanche") && risk.equals("2")){
            LocationDrawable = R.drawable.snowavalanche_r2_map;
        }
        if(category.equals("snowavalanche") && risk.equals("3")){
            LocationDrawable = R.drawable.snowavalanche_r3_map;
        }
        if(category.equals("snowavalanche") && risk.equals("4")){
            LocationDrawable = R.drawable.snowavalanche_r4_map;
        }
        if(category.equals("snowavalanche") && risk.equals("5")){
            LocationDrawable = R.drawable.snowavalanche_r5_map;
        }
        if(category.equals("snowstorm") && risk.equals("1")){
            LocationDrawable = R.drawable.snowstorm_r1_map;
        }
        if(category.equals("snowstorm") && risk.equals("2")){
            LocationDrawable = R.drawable.snowstorm_r2_map;
        }
        if(category.equals("snowstorm") && risk.equals("3")){
            LocationDrawable = R.drawable.snowstorm_r3_map;
        }
        if(category.equals("snowstorm") && risk.equals("4")){
            LocationDrawable = R.drawable.snowstorm_r4_map;
        }
        if(category.equals("snowstorm") && risk.equals("4")){
            LocationDrawable = R.drawable.snowstorm_r4_map;
        }
        if(category.equals("strike") && risk.equals("1")){
            LocationDrawable = R.drawable.strike_r1_map;
        }
        if(category.equals("strike") && risk.equals("2")){
            LocationDrawable = R.drawable.strike_r2_map;
        }
        if(category.equals("strike") && risk.equals("3")){
            LocationDrawable = R.drawable.strike_r3_map;
        }
        if(category.equals("strike") && risk.equals("4")){
            LocationDrawable = R.drawable.strike_r4_map;
        }
        if(category.equals("strike") && risk.equals("5")){
            LocationDrawable = R.drawable.strike_r5_map;
        }
        if(category.equals("stormsurge") && risk.equals("1")){
            LocationDrawable = R.drawable.stormsurge_r1_map;
        }
        if(category.equals("stormsurge") && risk.equals("2")){
            LocationDrawable = R.drawable.stormsurge_r2_map;
        }
        if(category.equals("stormsurge") && risk.equals("3")){
            LocationDrawable = R.drawable.stormsurge_r3_map;
        }
        if(category.equals("stormsurge") && risk.equals("4")){
            LocationDrawable = R.drawable.stormsurge_r4_map;
        }
        if(category.equals("stormsurge") && risk.equals("5")) {
            LocationDrawable = R.drawable.stormsurge_r5_map;
        }
        if(category.equals("technicaldisaster") && risk.equals("1")){
            LocationDrawable = R.drawable.technicaldisaster_r1_map;
        }
        if(category.equals("technicaldisaster") && risk.equals("2")){
            LocationDrawable = R.drawable.technicaldisaster_r2_map;
        }
        if(category.equals("technicaldisaster") && risk.equals("3")){
            LocationDrawable = R.drawable.technicaldisaster_r3_map;
        }
        if(category.equals("technicaldisaster") && risk.equals("4")){
            LocationDrawable = R.drawable.technicaldisaster_r4_map;
        }
        if(category.equals("technicaldisaster") && risk.equals("5")){
            LocationDrawable = R.drawable.technicaldisaster_r5_map;
        }
        if(category.equals("terror") && risk.equals("1")){
            LocationDrawable = R.drawable.terror_r1_map;
        }
        if(category.equals("terror") && risk.equals("2")){
            LocationDrawable = R.drawable.terror_r2_map;
        }
        if(category.equals("terror") && risk.equals("3")){
            LocationDrawable = R.drawable.terror_r3_map;
        }
        if(category.equals("terror") && risk.equals("4")){
            LocationDrawable = R.drawable.terror_r4_map;
        }
        if(category.equals("terror") && risk.equals("5")){
            LocationDrawable = R.drawable.terror_r5_map;
        }
        if(category.equals("tornado") && risk.equals("1")){
            LocationDrawable = R.drawable.tornado_r1_map;
        }
        if(category.equals("tornado") && risk.equals("2")){
            LocationDrawable = R.drawable.tornado_r2_map;
        }
        if(category.equals("tornado") && risk.equals("3")){
            LocationDrawable = R.drawable.tornado_r3_map;
        }
        if(category.equals("tornado") && risk.equals("4")){
            LocationDrawable = R.drawable.tornado_r4_map;
        }
        if(category.equals("tornado") && risk.equals("5")){
            LocationDrawable = R.drawable.tornado_r5_map;
        }
        if(category.equals("transportaccident") && risk.equals("1")){
            LocationDrawable = R.drawable.transportaccident_r1_map;
        }
        if(category.equals("transportaccident") && risk.equals("2")){
            LocationDrawable = R.drawable.transportaccident_r2_map;
        }
        if(category.equals("transportaccident") && risk.equals("3")){
            LocationDrawable = R.drawable.transportaccident_r3_map;
        }
        if(category.equals("transportaccident") && risk.equals("4")){
            LocationDrawable = R.drawable.transportaccident_r4_map;
        }
        if(category.equals("transportaccident") && risk.equals("5")){
            LocationDrawable = R.drawable.transportaccident_r5_map;
        }
        if(category.equals("transportincident") && risk.equals("1")){
            LocationDrawable = R.drawable.transportincident_r1_map;
        }
        if(category.equals("transportincident") && risk.equals("2")){
            LocationDrawable = R.drawable.transportincident_r2_map;
        }
        if(category.equals("transportincident") && risk.equals("3")){
            LocationDrawable = R.drawable.transportincident_r3_map;
        }
        if(category.equals("transportincident") && risk.equals("4")){
            LocationDrawable = R.drawable.transportincident_r4_map;
        }
        if(category.equals("transportincident") && risk.equals("5")){
            LocationDrawable = R.drawable.transportincident_r5_map;
        }
        if(category.equals("travelwarning") && risk.equals("1")){
            LocationDrawable = R.drawable.travelwarning_r1_map;
        }
        if(category.equals("travelwarning") && risk.equals("2")){
            LocationDrawable = R.drawable.travelwarning_r2_map;
        }
        if(category.equals("travelwarning") && risk.equals("3")){
            LocationDrawable = R.drawable.travelwarning_r3_map;
        }
        if(category.equals("travelwarning") && risk.equals("4")){
            LocationDrawable = R.drawable.travelwarning_r4_map;
        }
        if(category.equals("travelwarning") && risk.equals("5")){
            LocationDrawable = R.drawable.travelwarning_r5_map;
        }
        if(category.equals("tropicalcyclone") && risk.equals("1")){
            LocationDrawable = R.drawable.tropicalcyclone_r1_map;
        }
        if(category.equals("tropicalcyclone") && risk.equals("2")){
            LocationDrawable = R.drawable.tropicalcyclone_r2_map;
        }
        if(category.equals("tropicalcyclone") && risk.equals("3")){
            LocationDrawable = R.drawable.tropicalcyclone_r3_map;
        }
        if(category.equals("tropicalcyclone") && risk.equals("4")){
            LocationDrawable = R.drawable.tropicalcyclone_r4_map;
        }
        if(category.equals("tropicalcyclone") && risk.equals("5")){
            LocationDrawable = R.drawable.tropicalcyclone_r5_map;
        }
        if(category.equals("tsunami") && risk.equals("1")){
            LocationDrawable = R.drawable.tsunami_r1_map;
        }
        if(category.equals("tsunami") && risk.equals("2")){
            LocationDrawable = R.drawable.tsunami_r2_map;
        }
        if(category.equals("tsunami") && risk.equals("3")){
            LocationDrawable = R.drawable.tsunami_r3_map;
        }
        if(category.equals("tsunami") && risk.equals("4")){
            LocationDrawable = R.drawable.tsunami_r4_map;
        }
        if(category.equals("tsunami") && risk.equals("5")){
            LocationDrawable = R.drawable.tsunami_r5_map;
        }
        if(category.equals("urbanfire") && risk.equals("1")){
            LocationDrawable = R.drawable.urbanfire_r1_map;
        }
        if(category.equals("urbanfire") && risk.equals("2")){
            LocationDrawable = R.drawable.urbanfire_r2_map;
        }
        if(category.equals("urbanfire") && risk.equals("3")){
            LocationDrawable = R.drawable.urbanfire_r3_map;
        }
        if(category.equals("urbanfire") && risk.equals("4")){
            LocationDrawable = R.drawable.urbanfire_r4_map;
        }
        if(category.equals("urbanfire") && risk.equals("5")){
            LocationDrawable = R.drawable.urbanfire_r5_map;
        }
        if(category.equals("volcanicash") && risk.equals("1")){
            LocationDrawable = R.drawable.volcanicash_r1_map;
        }
        if(category.equals("volcanicash") && risk.equals("2")){
            LocationDrawable = R.drawable.volcanicash_r2_map;
        }
        if(category.equals("volcanicash") && risk.equals("3")){
            LocationDrawable = R.drawable.volcanicash_r3_map;
        }
        if(category.equals("volcanicash") && risk.equals("4")){
            LocationDrawable = R.drawable.volcanicash_r4_map;
        }
        if(category.equals("volcanicash") && risk.equals("5")){
            LocationDrawable = R.drawable.volcanicash_r5_map;
        }
        if(category.equals("volcanoactivity") && risk.equals("1")){
            LocationDrawable = R.drawable.volcanoactivity_r1_map;
        }
        if(category.equals("volcanoactivity") && risk.equals("2")){
            LocationDrawable = R.drawable.volcanoactivity_r2_map;
        }
        if(category.equals("volcanoactivity") && risk.equals("3")){
            LocationDrawable = R.drawable.volcanoactivity_r3_map;
        }
        if(category.equals("volcanoactivity") && risk.equals("4")){
            LocationDrawable = R.drawable.volcanoactivity_r4_map;
        }
        if(category.equals("volcanoactivity") && risk.equals("5")){
            LocationDrawable = R.drawable.volcanoactivity_r5_map;
        }
        if(category.equals("wildfire") && risk.equals("1")){
            LocationDrawable = R.drawable.wildfire_r1_map;
        }
        if(category.equals("wildfire") && risk.equals("2")){
            LocationDrawable = R.drawable.wildfire_r2_map;
        }
        if(category.equals("wildfire") && risk.equals("3")){
            LocationDrawable = R.drawable.wildfire_r3_map;
        }
        if(category.equals("wildfire") && risk.equals("4")){
            LocationDrawable = R.drawable.wildfire_r4_map;
        }
        if(category.equals("wildfire") && risk.equals("5")){
            LocationDrawable = R.drawable.wildfire_r5_map;
        }
        if(LocationDrawable ==R.drawable.icon_rot){
            int debug=0;
            int a = debug++;
        }
        return LocationDrawable;
    }
}
